# -*- coding: utf-8 -*-
require 'videosite/scraper'

RSpec.describe Videosite::Scraper do
  subject(:scraper) { Videosite::Scraper.new('html')}

  context '動画サイト ダイレクト' do
    it "youtube w0198u0mEc" do
      scraper.url = 'https://www.youtube.com/watch?v=w0198u0mEc'
      scraper.html = ''
      result =  scraper.scrape
      expect(result[0][:site_symbol]).to eq :youtube
      expect(result[0][:param]).to eq 'w0198u0mEc'
    end
  end

  context 'emmbedリンクを含むページ' do
    it "youtube yzC4hFK5P3g" do
      scraper.url = 'http://examplevideo.com'
      scraper.html = <<"HTML"
<title>動画のタイトル</title>
<iframe width="560" height="315" src="//www.youtube.com/embed/yzC4hFK5P3g?list=RDHCNUTXhDRWkCI" frameborder="0" allowfullscreen></iframe>
HTML
      result =  scraper.scrape
      expect(result[0][:site_symbol]).to eq :youtube
      expect(result[0][:param]).to eq 'yzC4hFK5P3g'
    end

  end
end
