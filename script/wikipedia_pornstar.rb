# -*- coding: utf-8 -*-

require 'optparse'
require 'logger'

require File.expand_path(File.dirname(__FILE__) + '/../lib/wikipedia/pornstar')

@log = Logger.new(STDOUT)
STDOUT.sync = true    

def import(pornstar)
  @log.info(pornstar[:name] + ' importing...')
  Actress.where(:name => pornstar[:name]).first_or_create do |p|
    p.yomi = pornstar[:yomi]
    p.last_name = pornstar[:last_name]
    p.first_name = pornstar[:first_name]
  end
end

opt = OptionParser.new
opt.on('-m [VALUE]') do |v|
  wikipedia = Wikipedia::Pornstar.new
  case v
  when 'download'
    @log.info('download started')
    wikipedia.download
    @log.info('download finished')
  when 'import'
    wikipedia.export_as_array.each do |pornstar|
      import(pornstar)
    end
  end
end

opt.parse!(ARGV)

