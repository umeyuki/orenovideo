# -*- coding: utf-8 -*-
require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"

# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Auth
  class Application < Rails::Application
    config.generators.template_engine = :slim
    config.generators.test_framework  = :rspec
    config.generators.stylesheets = false
    config.generators.javascripts = false
    config.generators.helper      = false
    config.autoload_paths += %W(#{config.root}/lib)
    config.secret_key_base = 'aweoiaeln'
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.precompile +=
      ["fontawesome-webfont.ttf",
      "fontawesome-webfont.eot",
      "fontawesome-webfont.svg",
      "fontawesome-webfont.woff"]
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :ja
    # config.middleware.use Rack::SslEnforcer, :except => [ /\/$/ ], :only => ['/login','/signup']
  end
end
