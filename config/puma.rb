root = "/export/orenovideo/current"
bind 'unix:///var/run/orenovideo/puma.sock'
pidfile "#{root}/tmp/pids/puma.pid"
state_path "#{root}/tmp/state"
daemonize true
activate_control_app
