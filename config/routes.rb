Auth::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with 'rake routes'.
  resources :users
  get 'signup' => 'users#new'

  resource :user_session, only: :create

  get 'login' => 'user_sessions#new'
  get 'logout' => 'user_sessions#destroy'
  delete 'logout' => 'user_sessions#destroy'

  # You can have the root of your site routed with 'root'
  resource :videos, :path => 'video'
  root 'roots#index'

  post 'multi_destroy' => 'videos#multi_destroy'

  get 'watch'     => 'videos#watch'
  get 'watch/:id'     => 'videos#watch'

  # get 'video/edit/:id' => 'videos#edit'
  get 'video/:site_symbol/:param' => 'videos#edit'

  get 'not_found' => 'static_pages#not_found'
  get 'demo' => 'static_pages#demo'
  get 'terms' => 'static_pages#terms'
  get 'nend' => 'static_pages#nend'

  post   'tags'  => 'tags#update'
  patch  'tags'  => 'tags#update'
  delete 'tags'  => 'tags#destroy'


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
