$('#bm-text').bind('touchstart', function() {
    this.focus();
    this.select();
});

function updateVideo(f,m){
    $.ajax({
        url: '/video',
        type: m,
        dataType: 'json',
        timeout: 1000,
        data : $(f).serialize(),
        beforeSend : function(){
            $('#post_status').html('送信中...').fadeIn(200);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            console.log(XMLHttpRequest)
            var message = submitText(m)
            alertify.error(message + "に失敗しました):");
        },
        success: function(obj){
            var form_num = 0;
            if (obj['form_num'] ){
                form_num = obj['form_num'];
            }
            form = '#form_' + form_num;
            $(form).attr('onsubmit', "return updateVideo(this," + "'PATCH')");

            $(form + ' input[type="submit"]').val('更新');
            var message = submitText(m)
            alertify.success(message + "しました(:");
        }
    });
    return false;
}


function submitText(m) {
    var text = (m === 'POST') ? '登録' : '更新';
    return text;
}

function submitTextR(m) {
    var text = (m === 'PATCH') ? '更新' : '登録';
    return text;
}

function updateTag(form_id,tag,m ) {
    $.ajax({
        url: '/tags',
        type: m,
        dataType: 'json',
        timeout: 1000,
        data : $('#form_'+ form_id).serialize() + "&tag=" + tag ,
        beforeSend : function(){
            $('#post_status').html('送信中...').fadeIn(200);
        },
        error: function(){
        },
        success: function(obj){
        }
    });
    return false;

}

function confirmDelete (){
    if($('input[group="video_id"]:checked').length && window.confirm('選択した動画を削除してよいですか?')){
        $('#destroy').submit;
    }
    return false;
}
