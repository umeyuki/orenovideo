class User < ActiveRecord::Base
  has_many :user_videos

  acts_as_authentic do |c|
    c.login_field = :login
  end
end
