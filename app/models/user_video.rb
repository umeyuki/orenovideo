class UserVideo < ActiveRecord::Base
  validates :title, presence: true

  belongs_to :user
  belongs_to :video
  has_many :tags
end
