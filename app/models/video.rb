class Video < ActiveRecord::Base
  has_many :user_videos
  has_one :thumbnail
end
