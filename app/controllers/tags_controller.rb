class TagsController < ApplicationController

  before_action do
    @user_video = Video.where(video_params).first.user_videos.where(user_id: current_user.id).first
  end

  def update
    Tag.create(
      name: params[:tag],
      user_video_id: @user_video.id,
      user_id: current_user.id
      )
    render :json => {:status => 'success' }
  rescue
    render :json => {:status => 'error' }
  end

  def destroy
    Tag.where(name: params[:tag],user_video_id: @user_video.id, user_id: current_user.id ).destroy_all
    render :json => {:status => 'success' }
  rescue
    render :json => {:status => 'error' }
  end

  private

  def video_params
        params[:video].permit(:site_symbol,:param)
  end


end
