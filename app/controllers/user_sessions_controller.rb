# -*- coding: utf-8 -*-
class UserSessionsController < ApplicationController
  skip_before_filter :require_login, only: [:new, :create]
  force_ssl :expect => [:destroy] if ENV['RACK_ENV'] == 'production'

  # GET /login
  def new
    session[:url] = params[:url]
    @user_session = UserSession.new
  end

  # POST /user_session
  def create
    @user_session = UserSession.new(user_session_params)
    if @user_session.save
      if url = session.delete(:url)
        redirect_to root_url(url: url )
      else
        redirect_to root_url
      end
    else
      @notice = "ログインに失敗しました"
      render action: :new
    end
  end

  # DELETE /logout
  def destroy
    current_user_session.destroy
    redirect_to login_url
  end

  private

    def user_session_params
      params.require(:user_session).permit(:login, :password)
    end
end
