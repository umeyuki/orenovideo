# -*- coding: utf-8 -*-
class StaticPagesController < ApplicationController
  before_filter :require_login, except: [:about,:qa,:terms,:demo,:not_found,:nend]
  def not_found

  end

  def terms
  end

  def demo
  end

  def nend
  end
end
