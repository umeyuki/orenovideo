# -*- coding: utf-8 -*-
require 'uri'
class VideosController < ApplicationController
  skip_before_filter :require_login, only: [:index,:show]
  before_action :new_video
  before_action :ajax?, only: [:create,:destroy]
  before_action :smartphone?, only:[:watch,:show]
  skip_before_filter :verify_authenticity_token ,:only => [:multi_destroy]
  before_filter  :redirect_to_http


  def show
    @current_user = current_user
    return if params[:url].empty?

    if url = params[:url]
      unless url.match(%r{\Ahttp})
        redirect_to root_path
        return
      end

      r = redis
      videos = {}
      if r.exists "orenovideo::url::#{url}"
        videos = eval r.get "orenovideo::url::#{url}"
      else
        videos = Videosite::Scraper.new(URI.encode(url)).scrape
        r.set "orenovideo::url::#{url}", videos
        r.expire "orenovideo::url::#{url}", 86400
      end
      if videos.empty?
        redirect_to '/not_found'
        return
      end

      videos.each do |v|
        v[:embed] = Videosite::Embedder.new(site_symbol: v[:site_symbol], param: v[:param]).embed
        actress_name = ''
        r = redis
        if  r.lrange('orenovideo::actresses',0,-1).empty?
          names = ['該当なし']
          Actress.all.each do |actress|
            names.push(actress.name)
            if v[:title].include?(actress.name)
              actress_name = actress.name
            end
          end
          # expire for new actress
          r.lpush('orenovideo::actresses',names)
          r.expire 'orenovideo::actresses', 86400
        else
          r.lrange('orenovideo::actresses',0,-1).each do |actress|
            if v[:title].include?(actress)
              actress_name = actress
            end
          end
        end
        if actress_name.empty?
          v[:actress_name] = '該当なし'
        else
          v[:actress_name] = actress_name
        end
        if logged_in?
          video = Video.select('*').where(param: v[:param],site_symbol: v[:site_symbol]).first_or_create
          user_video = UserVideo.joins(:video).where(video_id: video.id,user_id: @current_user.id).first
          if  user_video  then
            v[:video] =  user_video
            tags = Tag.where(user_video_id: user_video.id)
            tag_list = []
            if tags
              tags.each do |t|
                tag_list << t.name
              end
              v[:tags] =  tag_list.join(',')
            end
          end
        end
        if  r.exists 'orenovideo::actresses::all::json'
          @actress_select_options = eval r.get 'orenovideo::actresses::all::json'
        else
          actress_select_options = ['該当なし']
          Actress.all.each do |actress|
            options = []
            options.push(actress.name)
            actress_select_options.push(options)
          end
          @actress_select_options = actress_select_options
          r.set 'orenovideo::actresses::all::json', @actress_select_options
          r.expire 'orenovideo::actresses::all::json', 86400
        end
        @videos = videos
      end
    end
  end

  def create
    v_params = video_params
    @new_video = Video.where(v_params).first_or_create
    @user_video = UserVideo.where(user_video_params.merge(user_id: current_user.id,video_id: @new_video.id)).first_or_create
    if @user_video.valid?
      RegisterThumbnail.perform_async(v_params.merge(video_id: @user_video.video.id))
      update_tag(@user_video.id, params[:tags])
      render :json => {:succes => 1,  :form_num => params[:form_num] }
      return
    end
    render :json => {:error => @user_video.errors }, :status => 422
    return
  end

  def edit
    return redirect_to '/404.html'unless valid_symbol(params[:site_symbol])
    video = Video.find_or_create_by(site_symbol: params[:site_symbol],param: params[:param])
    @embed = Videosite::Embedder.new(site_symbol: params[:site_symbol], param: params[:param]).embed

    @user_video = video.user_videos.where(user_id: current_user.id).first_or_create
    r = redis
    if  r.exists 'orenovideo::actresses::all::json'
      @actress_select_options = eval r.get 'orenovideo::actresses::all::json'
    else
      actress_select_options = []
      Actress.all.each do |actress|
        options = []
        options.push(actress.name)
        actress_select_options.push(options)
      end
      @actress_select_options = actress_select_options
      r.set 'orenovideo::actresses::all::json', @actress_select_options
      r.expire 'orenovideo::actresses::all::json', 86400

    end
  end


  def update
    if @new_video = Video.where(video_params).first_or_create
      @user_video = UserVideo.find_or_initialize_by(user_id: current_user.id,video_id: @new_video.id)
      @user_video.update_attributes(user_video_params)
      update_tag(@user_video.id, params[:tags])
      render :json => {:succes => 1,  :form_num => params[:form_num] }
    else
      render :json => {:error => @new_videos.errors }
    end
  end

  def destroy
    video_id = Video.where(video_params).first.id
    @destroy_video = UserVideo.where(video_id: video_id,user_id: current_user.id).destroy_all

    if @destroy_video
      Tag.destroy_all(user_video_id: @destroy_video.shift.id)
      render :json => {:succes => 1, :form_num => params[:form_num] }
    else
      render :json => {:error => @destroy_video.errors }
    end
  end

  def watch
    if params[:id]
      user_video = UserVideo.where(id: params[:id], user_id: current_user.id ).first.serializable_hash.symbolize_keys
      video = Video.find(user_video[:video_id])
      user_video[:embed] = Videosite::Embedder.new(site_symbol: video[:site_symbol], param: video[:param]).embed
      @video = user_video
      # correct?
      # r = redis

      # if r.exists("dmm::" + user_video[:actress_name])
      #   @dmm_items = eval(r.get("dmm::" + user_video[:actress_name]) )
      #   @actress_name = user_video[:actress_name]
      # else
      #   @dmm_items = Dmm::Product.lookup(name: user_video[:actress_name],api_id: APP_CONFIG['dmm']['api_id'],affiliate_id: APP_CONFIG['dmm']['affiliate_id'])
      #   @actress_name = NKF.nkf('-w',user_video[:actress_name])
      #   r.set("dmm::" + @actress_name, @dmm_items) 
      # end
      
    else
      
      actress_select_options = []
      UserVideo.joins(:video).where(user_id: current_user.id).group('actress_name').each do |actress|
        actress_select_options.push('',actress.actress_name)
      end
      @actress_select_options = actress_select_options
      @tags = Tag.joins(:user_video).merge(UserVideo.where(user_id: current_user.id)).group('name')
      # Order.joins(:user).merge(User.where(:name=>'viva'))
      video_cond = { user_id: current_user.id }

      if actress_name = params[:actress_name]
        video_cond[:actress_name] = actress_name
      end

      if params[:query] && !params[:query].empty?
        video_cond = ["user_id = ? and title like ?", current_user.id, "%#{params[:query]}%"]
      end

      if tag = params[:tag]
        user_video_ids = Tag.where(name: tag,user_video_id: @tags.pluck(:user_video_id)  ).pluck(:user_video_id)
        video_cond[:id] = user_video_ids
      end

      myvideo_ids = UserVideo.where(video_cond).pluck(:video_id)
      @videos= UserVideo.where(video_cond).order('created_at DESC').page(params[:page]).per(5)
      @user_video = UserVideo.new
      render 'list'
    end
  end

  def multi_destroy
    UserVideo.destroy_all(id:params[:video_id], user_id: current_user.id)
    redirect_to watch_path
  end

  private

  def valid_symbol(site_symbol)
    %w{youtube youku vimeo xhamster fc2 xvideos redtube asg}.any? do |s|
      s == site_symbol
    end
  end

  def new_video
    @video = Video.new
  end

  def update_tag(user_video_id,tag)
      Tag.destroy_all(user_video_id: user_video_id)
      if  tag
        tags = tag.split(',')
        case tags
        when String
          Tag.create(name: tags,user_video_id: user_video_id)
        when Array
          values = []
          tags.each do |tag|
            values << Tag.new(name: tag,user_video_id: user_video_id)
          end
          Tag.import values
        end
      end
  end

  def video_params
    video_params = params[:video].permit(:site_symbol,:param)
    unless video_params[:site_symbol]
      video_params = params[:user_video].permit(:site_symbol,:param)
    end
    video_params
  end

  def user_video_params
    if params[:video][:title]
      return  params[:video].permit(:title,:actress_name)
    else
      return  params[:user_video].permit(:title,:actress_name)
    end
  end

  def delete_video_params
    params[:video].permit(:site_symbol,:param,:title,:user_id)
  end

  def ajax?
    return redirect_to '/404.html' unless request.xhr?
  end


end
