class RootsController < ApplicationController
  skip_before_filter :require_login, :only => :index
  before_filter  :redirect_to_http
  def index

    if params[:url]
      redirect_to videos_path(url: params[:url] )
    end
  end
end
