# -*- coding: utf-8 -*-
class UsersController < ApplicationController
  skip_before_filter :require_login, only: [:new, :create]


  # GET /signup
  def new
    if logged_in?
      redirect_to new_videos_path
    end
    @user = User.new
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if verify_recaptcha(:model => @user, :message => '画像の文字と一致しません') && @user.save
      redirect_to root_url , :only_path => false, :protocol => 'http://'
      return
    else
      render action: :new
    end
  end


  private

  def user_params
    params.require(:user).permit(:login, :password, :password_confirmation)
  end
end
