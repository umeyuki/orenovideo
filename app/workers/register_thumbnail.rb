class RegisterThumbnail
  include Sidekiq::Worker
  sidekiq_options queue: :thumbnail, retry: false
  def perform(params)
    logger = Logger.new(File.join(Rails.root, 'log', 'thumbnails.log'))
    origin_url = Videosite::Url.new(site_symbol: params['site_symbol'],param: params['param']).url
    thumb_url = Videosite::Thumbnail.new(site_symbol: params['site_symbol'],param: params['param'],url: origin_url).get
    logger.info "site_symbol:#{params['site_symbol']}\tparam:#{params['param']}\tthumb_url:#{thumb_url}}}"
    thumbnail = Thumbnail.where(video_id: params['video_id'],url: thumb_url).first_or_create
    logger.close
  end
end
