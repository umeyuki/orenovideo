# -*- coding: utf-8 -*-
require 'ruby-dmm'

module Dmm
  class Product
    def self.lookup(name:,api_id:,affiliate_id:)
      client = DMM.new(:api_id => api_id,:affiliate_id => affiliate_id, :result_only => true)
      result = client.order("rank").order("date").limit(4).item_list(name, site: 'DMM.co.jp', service: 'digital', floor:'videoa' )
      # res = client.order("date").limit(3).item_list(@actress)
      item_list = []
      result.items.map {  |item|
        item_list.push :affiliate_url => item.affiliate_url, :image_url => item.image_url.large, :title => item.title.force_encoding('utf-8')
      }
      item_list
    end
    # 女優名を入れたら関連する
  end
end
