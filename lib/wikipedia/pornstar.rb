# -*- coding: utf-8 -*-
require 'open-uri'
require 'nokogiri'
require 'logger'

class Wikipedia
  class Pornstar
    attr_accessor :pornstars
    
    def initialize(dir:File.expand_path(File.dirname(__FILE__) + '/../../log/pages'))
      @log = Logger.new(STDOUT)
      STDOUT.sync = true    
      @dir = dir
      @wiki_url = "http://ja.wikipedia.org"
      @pornstars = []
    end

    def download
      begin
        index = open(@wiki_url + "/wiki/AV%E5%A5%B3%E5%84%AA%E4%B8%80%E8%A6%A7");
        page = Nokogiri::HTML(index.read, nil, 'UTF-8')
        page.css('#mw-content-text ul li').each do |node|
          link = node.child
          if link.text =~ /AV女優一覧/
            file_name = link.text.split(' ')[1]
            @log.info(file_name + ' now downloading...')
            download_dir =  "#{@dir}/#{file_name}\.html"
            open(download_dir,'wb') do | file |
              open(@wiki_url + link['href']) do |data|
                file.write(data.read)          #※２
                @log.info(file_name + ' download finished')
              end
            end
          end
        end
      rescue => ex
        abort ex.message
      end
    end

    def export_as_array
      Dir.entries(@dir).each do |file|
        next if file == '.' || file == '..'
        html_as_array("#{@dir}/#{file}")        
      end     
      self.pornstars
    rescue  => ex
      @log.error(ex.message)      
    end

    private
    def html_as_array(html)
      entry = Nokogiri::HTML(open(html).read, nil, 'UTF-8')
      entry.css('h3+ul li').each do |node|
        next unless node['class'].nil? && node['id'].nil?
        pornstar = {}
        pornstar[:name] = node.child.text
        if /(([ぁ-ん]+)\s+([ぁ-ん]+))/ =~ node.text then
          pornstar[:yomi] = $1
          pornstar[:last_name] = $2
          pornstar[:first_name] = $3
        end
        self.pornstars.push(pornstar)
      end
    end
  end
end
