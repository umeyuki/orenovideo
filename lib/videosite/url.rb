module Videosite
  class Url
    def initialize(site_symbol:,param:)
      @site_symbol = site_symbol
      @param        = param
    end
    def url
      __send__ @site_symbol
    end

    private

    def youtube
      "https://www.youtube.com/watch?v=%s"%[@param]
    end

    def xvideos
      "http://www.xvideos.com/video%s"%[@param]
    end

    def fc2
      "http://video.fc2.com/ja/content/%s"%[@param]
    end

    # def xhamster

    # end

    def redtube
      "http://www.redtube.com/%s"
    end

    def asg
      "http://asg.to/contentsPage.html?mcd=%s"%[@param]
    end
  end
end
