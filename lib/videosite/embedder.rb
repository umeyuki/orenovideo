# -*- coding: utf-8 -*-
module Videosite
  class Embedder
    FC2_AFFILIATE_ID = 'TkRVNE1qRTFPRFk9'.freeze
    def initialize(site_symbol:,param:)
      @site_symbol = site_symbol
      @param = param
    end

    def embed
      __send__(@site_symbol)
    end

    private
    def youtube
      "<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/%s\" frameborder=\"0\" allowfullscreen></iframe>"%[@param]
    end

    def youku
      "<iframe height=498 width=510 src=\"http://player.youku.com/embed/%s\" frameborder=0 allowfullscreen></iframe>"%[@param]
    end

    # def dailymotion
    #   "<iframe frameborder=\"0\" width=\"#{@width}\" height=\"#{@height}\" src=\"http://www.dailymotion.com/embed/video/%s\" allowfullscreen></iframe>"%[@param]
    # end

    def xvideos
      "<iframe src=\"http://flashservice.xvideos.com/embedframe/%s\" frameborder=0 width=510 height=400 scrolling=no></iframe>"%[@param]
    end

    def fc2
      "<div class=\"player\">
<script src=\"http://static.fc2.com/video/js/outerplayer.min.js\" url=\"http://video.fc2.com/ja/a/content/%s/\" tk=\"%s\" tl=\"\" sj=\"49000\" d=\"914\" w=\"700\" h=\"400\"  charset=\"UTF-8\"></script>
</div>"%[@param,FC2_AFFILIATE_ID]
    end

    def redtube
      "<iframe src=\"http://embed.redtube.com/?id=%s&bgcolor=000000\" frameborder=\"0\" width=\"434\" height=\"344\" scrolling=\"no\"></iframe>"%[@param]
    end

    def xhamster
      # <iframe width="510" height="400" src="http://xhamster.com/xembed.php?video=2537045" frameborder="0" scrolling="no"></iframe>
      "<iframe width=\"510\" height=\"400\" src=\"http://xhamster.com/xembed.php?video=%s\" frameborder=\"0\" scrolling=\"no\"></iframe>"%[@param]
    end

    def asg
      # <script type="text/javascript" src="http://asg.to/js/past_uraui.js"></script><script type="text/javascript">Purauifla("mcd=S1aaoaiheg", 450, 372);</script>
      "<iframe id=\"\" src=\"http://asg.to/blogFrame.html?mcd=%s&amp;width=450&amp;height=372\" style=\"width: 450px; height: 372px; border: none;\" frameborder=\"no\" scrolling=\"no\"></iframe>"%[@param]
    end

    def vimeo
      # <iframe src="//player.vimeo.com/video/93585374" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/93585374">I DECIDED TO LEAVE</a> from <a href="http://vimeo.com/danbritt">Dan Britt</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
      "<iframe src=\"//player.vimeo.com/video/%s?byline=0&amp;portrait=0&amp;badge=0&amp;color=c2c2c2\" width=\"500\" height=\"281\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"%[@param]
    end

  end
end
