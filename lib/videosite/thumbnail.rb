# -*- coding: utf-8 -*-
require 'open-uri'
require 'open-uri'
require 'videosite/url'
require 'redtube'
require 'httparty'
require 'vimeo'
require 'json'

module Videosite
  class Thumbnail
    def initialize(site_symbol:,param:,url:)
      @site_symbol = site_symbol
      @param       = param
      @url         = url
    end

    def get
      __send__(@site_symbol)
    end

    def youtube
      'http://img.youtube.com/vi/%s/mqdefault.jpg'%[@param]
    end

    def vimeo
      vimeo = Vimeo::Simple::Video.info(@param)
      JSON.parse(vimeo.body)[0]['user_portrait_huge']
    end

    def xvideos
      response = HTTParty.get("http://api.erodouga-rin.net/thumbnails?url=%s"%[@url])
      if response.code == 200
        result = JSON.parse(response.body)
        if result['status'] == 'success'
          run = [*0..29].sample
          return result['thumbnails'][run]
        end
      end

      @origin_page = Nokogiri::HTML(open(@url).read, nil, 'UTF-8')
      @origin_page.css('#tabVote img').attr('src').value
    end

    def fc2
      begin
        @origin_page = Nokogiri::HTML(open(@url).read, nil, 'UTF-8')
        @origin_page.css('link[rel=image_src]').attr('href').value
      rescue OpenURI::HTTPError
        puts '404'
      end
    end

    def xhamster
      nil
    end

    def redtube
      Redtube::Video.find(@param).thumb
    end



    # def asg
    # 元ページ => タイトル取得 or タグ => 検索 最初の1件のサムネイルを取得
    # end
  end
end
