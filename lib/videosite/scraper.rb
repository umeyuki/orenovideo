require 'open-uri'
require 'uri'
module Videosite
  class Scraper
    attr_accessor :html,:url
    def initialize(thing)
      @embed_links = []
      begin
        @html = ''
        case thing
        when /\Ahttp:\/\//,/\Ahttps:\/\//
          @url = thing
          charset = nil
          @html = open(thing) do |f|
            charset = f.charset
            f.read
          end
        else
          @html = thing
        end
      rescue => ex
        puts ex.message
      end

      @origin_regexs = {
        youtube:   %r{//www\.youtube\.com/watch\?v=([0-9a-zA-Z_-]+)},
        vimeo:     %r{https://vimeo.com/([0-9]+)},
        youku:     %r{(?:https|http)://v\.youku.com/v_show/id_([0-9a-zA-Z_]+)\.html\?/*},
        xhamster:  %r{(?:https|http)://xhamster.com/movies/([0-9]+)/*},
        xvideos:   %r{(?:https|http)://www\.xvideos.com/video([0-9]+)/} ,
        fc2:       %r{(?:https|http)://video\.fc2\.com/(?:ja/content|en/content|content)/([0-9a-zA-Z]+)/},
        asg:       %r{(?:https|http)://asg.to/contentsPage.html\?mcd=([0-9a-zA-Z+]+)},
        redtube:   %r{(?:https|http)://www.redtube.com/([0-9]+)}
      }

      # http://video.fc2.com/flv2.swf?t=201407280507
      @regexs = {
        youtube:   %r{//www.youtube.com/(?:embed/|watch\?v=)([0-9a-zA-Z_-]+)},
        vimeo:     %r{player.vimeo.com/video/([0-9]+)},
        youku:     %r{//player.youku.com/embed/([0-9a-zA-Z_]+)},
        xhamster:  %r{//xhamster.com/xembed.php\?video=([0-9]+)},
        xvideos:   %r{//flashservice.xvideos.com/embedframe/([0-9]+)} ,
        fc2:       %r{//video.fc2.com/(?:flv2.swf\?i=|ja/a/content/|api/matome/video\.php\?upid=|flv2\.swf\?t=|a/content/)([0-9a-zA-Z]+)},
        asg:       %r{contentsPage.html\?mcd=([0-9a-zA-Z+]+)},
        redtube:   %r{//embed.redtube.com/player/\?id=([0-9]+)}
      }
    end

    def scrape
      unless @url.empty?
        scrape_origin
        unless @embed_links.empty?
          return @embed_links
        end
      end

      title = get_title
      if title
        title.force_encoding("utf-8")
        title = title.encode("UTF-16BE", "UTF-8", :invalid => :replace, :undef => :replace, :replace => '?').encode("UTF-8")
        title.gsub!(%r{([0-9a-zA-Z_]+)\.com},'')
        title.gsub(/[!-\/:-@\[-`{-~]/, "").gsub!(%r{\s{2,}},'') unless title.nil?
      end

      exist_key_map = {}
      @regexs.each do |site,regex|
        @html.scan(regex) do |m|
          next if exist_key_map["#{site}_#{m[0]}"]
          @embed_links.push site_symbol:  site, param:  m[0], title: title
          exist_key_map["#{site}_#{m[0]}"] = 1
        end
      end
      @embed_links
    end

    private

    def scrape_origin
      @origin_regexs.each do |site,regex|
        @url.scan(regex) do |m|
          @embed_links.push site_symbol: site, param: m[0], title: get_title
        end
      end
    end

    def get_title
      title = @html.scan(%r{<title>(.+)</title>})
      unless title.empty?
        return title[0][0]
      end

    end
  end
end
