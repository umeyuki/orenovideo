class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.integer :user_id
      t.string :actress_name
      t.string :title
      t.string :site_symbol
      t.string :param

      t.timestamps
    end
    add_index :videos, :actress_name
  end
end
