class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.integer :user_video_id
      t.string :name
      t.timestamps
    end
    add_index :tags, :name
  end
end
