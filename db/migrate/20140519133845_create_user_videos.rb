class CreateUserVideos < ActiveRecord::Migration
  def change
    create_table :user_videos do |t|
      t.integer :user_id
      t.integer :video_id
      t.string  :title
      t.string  :actress_name
      t.timestamps
    end
    add_index :user_videos, :user_id
    add_index :user_videos, :video_id
    add_index :user_videos, [:user_id, :video_id], unique: true
  end
end
