class AddIndexToTag < ActiveRecord::Migration
  def change
    add_index :tags,[ :user_video_id, :name ], unique: true
  end
end
