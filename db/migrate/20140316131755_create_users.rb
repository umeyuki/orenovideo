class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string        :login,               null: false, limit: 50
      t.string        :crypted_password,    null: false
      t.string        :password_salt,       null: false
      t.string        :persistence_token,   null: false

      t.integer       :login_count,         null: false, default: 0
      t.integer       :failed_login_count,  null: false, default: 0
      t.datetime      :current_login_at
      t.datetime      :last_login_at
      t.timestamps
    end
    add_index :users, [:login], unique: true
  end
end
