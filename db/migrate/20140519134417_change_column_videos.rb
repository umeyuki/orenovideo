class ChangeColumnVideos < ActiveRecord::Migration
  def change
    remove_column :videos, :user_id
    remove_column :videos, :title
    remove_column :videos, :actress_name
  end
end
