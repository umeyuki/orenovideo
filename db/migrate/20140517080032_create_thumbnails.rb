class CreateThumbnails < ActiveRecord::Migration
  def change
    create_table :thumbnails do |t|
      t.string :site_symbol
      t.string :param
      t.string :url

      t.timestamps
    end
  end
end
