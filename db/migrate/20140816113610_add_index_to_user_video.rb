class AddIndexToUserVideo < ActiveRecord::Migration
  def change
    add_index :user_videos,[ :user_id, :title ]
    add_index :user_videos,[ :user_id, :actress_name ]
  end
end
