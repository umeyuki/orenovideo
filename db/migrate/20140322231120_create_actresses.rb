class CreateActresses < ActiveRecord::Migration
  def change
    create_table :actresses do |t|
      t.string :name
      t.string :yomi
      t.string :last_name
      t.string :first_name
      t.timestamps
    end
  end
end
