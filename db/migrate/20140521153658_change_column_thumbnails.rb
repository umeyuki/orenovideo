class ChangeColumnThumbnails < ActiveRecord::Migration
  def change
    remove_column :thumbnails, :site_symbol
    remove_column :thumbnails, :param
    add_column    :thumbnails, :video_id, :integer
  end
end
