# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140816113610) do

  create_table "actresses", force: true do |t|
    t.string   "name"
    t.string   "yomi"
    t.string   "last_name"
    t.string   "first_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "search_urls", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.integer  "user_video_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "tags", ["name"], name: "index_tags_on_name"
  add_index "tags", ["user_id", "name"], name: "index_tags_on_user_id_and_name", unique: true
  add_index "tags", ["user_video_id"], name: "index_tags_on_user_video_id"

  create_table "thmbnails", force: true do |t|
    t.integer  "video_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "thmbnails", ["video_id"], name: "index_thmbnails_on_video_id"

  create_table "thumbnails", force: true do |t|
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "video_id"
  end

  create_table "user_videos", force: true do |t|
    t.integer  "user_id"
    t.integer  "video_id"
    t.string   "title"
    t.string   "actress_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_videos", ["user_id", "actress_name"], name: "index_user_videos_on_user_id_and_actress_name"
  add_index "user_videos", ["user_id", "title"], name: "index_user_videos_on_user_id_and_title"
  add_index "user_videos", ["user_id", "video_id"], name: "index_user_videos_on_user_id_and_video_id", unique: true
  add_index "user_videos", ["user_id"], name: "index_user_videos_on_user_id"
  add_index "user_videos", ["video_id"], name: "index_user_videos_on_video_id"

  create_table "users", force: true do |t|
    t.string   "login",              limit: 50,             null: false
    t.string   "crypted_password",                          null: false
    t.string   "password_salt",                             null: false
    t.string   "persistence_token",                         null: false
    t.integer  "login_count",                   default: 0, null: false
    t.integer  "failed_login_count",            default: 0, null: false
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["login"], name: "index_users_on_login", unique: true

  create_table "videos", force: true do |t|
    t.string   "site_symbol"
    t.string   "param"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
